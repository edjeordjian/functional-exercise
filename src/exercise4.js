const _addWord = (sequence, countedWords, idx1, idx2) => {
    word = sequence.substring(idx1, idx2 + 1).toLowerCase().trim();
    previous = countedWords[word] === undefined ? 0 : countedWords[word];
    countedWords[word] = previous + 1;
}

const _wordCount = (sequence, countedWords, idx1, idx2) => {
    if (sequence.length - 1 === idx2) {
        if ( ! (sequence[idx2] === ' ' && sequence[idx1] === ' ') ) {
            _addWord(sequence, countedWords, idx1, idx2);
        }

        return;
    }

    if (sequence[idx2] !== ' ' && sequence[idx2 + 1] === ' ') {
        _addWord(sequence, countedWords, idx1, idx2);
        idx1 = idx2 + 1;
    }

    _wordCount(sequence, countedWords, idx1, idx2 + 1);
};

const wordCount = (sequence) => {
    if (sequence === undefined || sequence.length === 0) {
        return null;
    }

    countedWords = {};
    _wordCount(sequence, countedWords, 0, 0);

    if ( Object.keys(countedWords).length == 0 ) {
        return null;
    }

    return countedWords;
};

module.exports = {
  wordCount,
};
