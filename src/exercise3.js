const fderive = (fn, h, x) => {
	return ( fn(x + h) - fn(x - h) ) / (2 * h);
}

const squear = x => {
	return x * x;
}

module.exports = {
  fderive,
  squear,
};
