const fsum = (numbers, idx) => {
	if (idx === numbers.length) {
		return 0;
	}

	return numbers[idx] + fsum(numbers, idx + 1);
}; 

const faverage = numbers => {
	if (numbers === undefined || numbers.length === 0) {
		return 0;
	}

	return fsum(numbers, 0) / numbers.length;
};

module.exports = faverage;
