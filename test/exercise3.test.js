const { fderive, squear } = require('../src/exercise3');

const assert = require('assert');

describe('fderive', function() {
	/*
	I have isolated the following piece of code, altogether with
	the corresponding functions to make it work:
	
	const derivefn = fderive(squear, 0.001);
    derivefn(2);
	
	And it produces the follwoing error: 
	
	TypeError: derivefn is not a function

	It is my understanding that the format of the call should be
	something like: 

	const derivefn = fderive(squear, 0.001, 2);
    derivefn;

	That is what I could come up with in order to complete the excercise.
	*/

  it('Approximate the derive of squear in point 2 with error 0.001.', () => {
    const derivefn = fderive(squear, 0.001, 2);
    assert.equal(derivefn, 3.9999999999995595);
  });

  it('Approximate the derive of squear in point 6 with error 0.001.', () => {
    const derivefn = fderive(squear, 0.001, 6);
    assert.equal(derivefn, 12.000000000004007);
  });
});
